# Spa-N-A-Box

**WARNING: I am not responsible for any damage you cause to your system. I am not an electrician nor am I qualified to provide any sort of advice. I am figuring it out as I go and will provide the knowledge that I find. Modifying your system is done at your own risk. Take any precautions necessary.**

*Note: I am NOT using any affiliate links or promoting any products.*

A gracious neighbor gifted me a Comfort Line Products Spa-N-A-Box that had broken down and would not set the temperature. Comfort Line Products went out of business so we are pretty much on our own to repair anything that breaks. I will continue to add to this README as I repair and improve what I have. I will also kindly accept any updates from others so that all the information is in the same place.

# Table of Contents

1. [Model Information](#ModelInfo)
1. [Pipe Caps](#PipeCaps)
1. [GFCI Plug](#GFCIPlug)
1. [Pump](#Pump)
1. [Pump Impeller](#PumpImpeller)
1. [Removing Cover](#RemovingCover)
1. [Rusted Buttons](#RustedButtons)
1. [Adding ESP32 To The Control Board](#AddingESP32)
1. [Replacing The Control Board](#ReplacingESP32)
1. [ESPHome and Home Assistant](#HomeAssistant)

## Model Information <a name="ModelInfo"></a>

My system version has the following information:

**Model:** CL250-C <br>
**Volts:** 120 <br>
**Amps:** 12 <br>
**Hertz:** 60 <br>
**Date of Mfg:** 17/10/2012 <br>

**Company Information Listed:** <br>
Comfort Line Products <br>
2717 North Tamiami Trail <br>
N. Ft. Myers, FL 33903 <br>
(239) 997-6366

## Pipe Caps<a name="PipeCaps"></a>

The first thing I did when I received the hot tub was put it all together and fill it up. As I was investigating the issues I realized I had to disconnect the system. The Spa-N-A-Box comes with 3 small caps for the pumps intake and return. I capped them and started disconnecting the pipes. The blower pipe began draining the hot tub from the blower line. I ended up getting a 2 in. PVC DWV Flexible Cap from HomeDepot which fit over the opening pretty well. [Home Depot Link](https://www.homedepot.com/p/Fernco-2-in-PVC-DWV-Flexible-Cap-PQC-102/100372303). It is probably not necessary since once the water drains below the blower line it should stop, but I bought it before I thought that part through.

This is what it looks like:

<img src="./images/2in-qwik-cap.jpg" alt="Blower Cap" width="30%"/>

It is a PlumbQwik by Fenco PQC-102 2" for cast iron and plastic.

## GFCI Plug <a name="GFCIPlug"></a>
The GFCI Plug was the first issue I ran into. In order for the pump to turn on it had to be held in a specific way. This indicated that something was loose inside. I took off the plug to inspect and noticed there was a leak with some rusty solder. Rather than trying to clean it up I opted to replace the plug end. 

The original plug looked like such:

<img src="./images/original-gfci-plug-front.jpg" alt="Original GFCI Plug Front" width="30%"/>

The inside of the plug looked looked like such:

<img src="./images/original-gfci-plug-inside.jpg" alt="Original GFCI Plug Inside" width="30%"/>

The inside of the plug wiring looked like such:

<img src="./images/original-gfci-plug-wired.jpg" alt="Original GFCI Plug Wired" width="30%"/>

The original specs of the plug are:

**Manufacturer:** Tower MFG. Corp.<br>
**Catalog No:** 30336 0912<br>
**LR:** 108160<br>
**Type:** Class A Rainproof GFCI<br>
**Watts:** 1875<br>
**Amps:** 15A<br>
**VAC:** 125<br>
**Hz:** 60<br>

I could not find the exact one from Tower MFG but I found one that met the specs located here: [Amazon Link](https://a.co/d/2OeSyzy). It was straight forward to switch the plug though it was a tight squeeze for the cord.

Here is the installed version:

<img src="./images/new-gfci-plug-front.jpg" alt="New GFCI Plug Front" width="30%"/>

The new specs of the plug are:

**Manufacturer:** Tower MFG. Corp.<br>
**Catalog No:** 30434<br>
**Type:** Class A Rainproof GFCI<br>
**Watts:** 1875<br>
**Amps:** 15A<br>
**VAC:** 125<br>
**Hz:** 60<br>

## Pump <a name="Pump"></a>

I noticed the pump would rattle and there was a small drip coming from the bottom of the pump. 

The pump on mine is listed as:

**Manufacturer:** HO LEE CO<br>
**Model:** HB-156B<br>
**VAC:** 120<br>
**Hz:** 60<br>

To take the pump off, there are 4 screws highlighted in green on the bottom. You also need to unscrew the connected pipe. There is a rubber gasket in the pipe to prevent it from leaking. Make sure not to lose this when putting it back together. 

<img src="./images/pump-bottom.jpg" alt="Pump Bottom" width="30%"/>
<img src="./images/pump-pipe.jpg" alt="Pump Pipe" width="30%"/>

Once I removed those the pump fell off. This should not happen. It was cracked at below the threaded impeller cover. The threading is held on by a gray ring inside the tank which can be removed with a flat head screwdriver by placing the edge into one of the 4 small dots (left dot is highlighted in green) and pulling counter-clockwise. Once the ring is unscrewed the pump should come off. The ring looks like the left picture:

<img src="./images/top-ring-on-pump.jpg" alt="Top Ring On Pump" width="30%"/>
<img src="./images/pump-ring.jpg" alt="Pump Ring" width="30%"/>

This is the removed pump. There is a small plastic pin that is located in the green highlight that you can remove. You can then twist to cover counter-clockwise to remove it. I had to use Alligator Pliers to hold get enough leverage to turn it. There are plastic holders that when rotated it slides out of. These are highlighted in red.

<img src="./images/impeller-cover-pin.jpg" alt="Impeller Cover Pin" width="30%"/>

<br><br>

I ended up super gluing the broken threaded impeller cover back together since I couldn't find a replacement. I highly doubt this will hold long term but for now it seems to do the trick. 

The part on the left is the closest cover I could find that resembles what is on the pump but it does not have the threading. I didn't purchase it to see if it fits.

<img src="./images/aquascape-impeller-cover.jpg" alt="Aquascape Impeller Cover" width="30%"/>

This image is for an Aquascape Front Cover Kit 550/800GPH Pump. Based on information from someone else about the impeller, I believe the pump may be an Aquascape 800 GPH pump or something similar.

I found what I believe is the same pump on Ebay (sold already) with the label on the side: SC3512-40750-80STW-04 19y15A </br> Unfortunatly there was not enough information on it to know who manufactured it or where to get them.

<img src="./images/ebay-pump.jpg" alt="Ebay Pump" width="30%"/>


## Pump Impeller <a name="PumpImpeller"></a>

After I took off the pump, I determined that the rattle was caused by a few fins being broken on the impeller and a little plastic piece was broken. 

This is what it looked like:

<img src="./images/broken-impeller.jpg" alt="Broken Impeller" width="30%"/>

I found on a comment on Amazon that someone purchased the Aquascape Ultra 800 Water Pump (G3) Replacement Impeller Kit (91041) which worked as a replacement. [Amazon Link](https://a.co/d/1E26G7e). I attempted to purchase this but the shipper never sent it and my order was cancelled. Since I was impatient, I ended up 3D Printing an impeller to match the size of the broken one. 

The stl file is located under stl-files/pump-impeller.stl in the repository. This is what it looks like:

<img src="./images/3d-printed-impeller.jpg" alt="3D Printed Impeller" width="30%"/>
<img src="./images/3d-printed-impeller-full.jpg" alt="3D Printed Impeller Full" width="30%"/>

It is designed to fit over the top of the cylinder with a tight fit. I did not secure it other than with friction and it has a longer base than the original in the picture above. This is the 3D printed impeller in place.

<img src="./images/impeller-in-place.jpg" alt="Impeller In Place" width="30%"/>


## Removing Cover <a name="RemovingCover"></a>

On the bottom of the pump there are a bunch of screws to take off. The ones highlighted in green are the blower screws. Take off the cover first before removing these. I didn't get a good picture of all of the cover screw, but they look like the ones highlighted in blue near the edges. The pump ones should be obvious and shouldn't be taken off to get the case off.

<img src="./images/blower-screws.jpg" alt="Blower Screws" width="30%" />

Once you remove the screws, the top cover can be pulled off. It is difficult due to the rubber gaskets. Here is a picture of both the cover and blower removed. I recommend leaving the blower screws in until the cover has been removed first. Be careful with the 7 wire band that may be wrapped around the blower tubes.

<img src="./images/cover-blower-removed.jpg" alt="Cover and Blower Removed" width="30%" />

This is the bottom of the blower which was held on the the 4 screws depicted in a previous picture.

<img src="./images/blower-bottom.jpg" alt="Blower Bottom" width="30%" />

Once the blower is removed, you can access the heating element here. I did not remove this to inspect it since mine is working. If anyone does, please send pictures and any information you may find so I can update the README. The green highlighted circles are where the two thermistors are located. I also did not remove these since they seem to work. The red highlighted circle is showing another repair I made. The plastic clamp had cracked and I replaced it with a metal one you can find at the hardware store.

<img src="./images/heater-element.jpg" alt="Heating Element" width="30%" />

This is the case for the system board which controls the heater and blower motor. There are two screw highlighted in red that you need to remove before the case will come out. There are 4 screws highlighted in green to open the case up. 

<img src="./images/system-board-case.jpg" alt="System Board Case" width="30%" />

This is what the back of the system board looks like once the case cover is removed.

<img src="./images/system-board-back.jpg" alt="System Board Back" width="30%" />

This is what the front of the system board looks like once pulled from the case. The bottom left of the picture is where the 7 wire connector attaches. It is removed in this picture for testing.

<img src="./images/system-board-front.jpg" alt="System Board Front" width="30%" />


## Rusted Buttons <a name="RustedButtons"></a>

The temperature would not set and continued to get reset to 100. I figured it had something to do with the board behind this:

<img src="./images/control-board-interface.jpg" alt="Control Board Interface" width="30%"/>
<br><br>

First I found that the water proof button interface has cracks which let small amounts of water in over time. This cause the physical buttons on the board to rust. This is what the board and rusted buttons looked like:

<img src="./images/rusted-buttons-control-board.jpg" alt="Control Board Interface" width="30%"/>

To get to the board, you need to remove the cover from the previous section. Once the cover is removed, on the inside there are two screws holding the control board case onto the cover. Once you remove those it should slip out. I did not take a picture of it, but you can see me adding it back in the ESP32 section.

This ended up being an easy fix by just replacing the buttons with two 6x6x5mm Micro Momentary Tactile Push Buttons. I just had a few laying around, but you can find them with most micro electronic starter kits. This is what they look like:

<img src="./images/push-buttons.jpg" alt="Push Buttons" width="30%"/>

In order to replace the buttons I had to scrape off the water proof coating on the solder points which I assume is clear silicon. I used an exacto knife for the larger chunks and evenually used the hot end of soldering iron which helped loosen it up. I then desoldered the remaining connections and wiped it down. After, it was simple to line them up in the same direction of the broken ones and solder them back on. In the picture you can see at the top where I removed the two buttons, desoldered and resoldered the new buttons.

<img src="./images/button-solder.jpg" alt="Push Buttons" width="30%"/>

After adding these buttons I plugged it back into the system to test. Rather than trying to heat the entire hot tub which would require that I have to put it all back together, I picked up two 1/2ID x 3/4MIP Hose Barbs since I had a 1/2 inch pipe. [Home Depot Link](https://www.homedepot.com/p/Everbilt-1-2-in-Barb-x-3-4-in-MIP-Nylon-Adapter-Fitting-800229/300862694). I connected it like such:

<img src="./images/hose-loop.jpg" alt="Hose Loop" width="30%"/>

This caused some issues with the flow or pressure sensor and caused the heating element to turn off. I was able to slow it down by pinching it off just enough like in the above picture for it to turn back on. I highly doubt this is a great thing to do but it worked and the temperature rose and maintained my set value.


## Adding ESP32 To The Existing Control Board <a name="AddingESP32"></a>

I have a bunch of ESP32 chips and thought it would be a good idea to connect one into the board while I have the pump disassembled. I run HomeAssistant to automate the house, and thought it would be useful to read and control the temperature remotely. The first step is understanding the board which looks relatively simple. I am a novice at hardware but I first tried to determine where I can tap into with the ESP.

I managed to figure out a few things on what I am calling the control board. The IC2 looks like it is a ATtiny26L version. In my schematic I made it an ATtiny26L-8SU since the last few characters looked like an SU. The 3 digit 7 segment display appears to be a E40521-1F by Toyo LED Electronics Limited. I couldn't find the 1F version but I found an LI version that is likely similar. [Link to PDF](https://www.toyo-led.com/wp-content/uploads/2017/02/E30521-LI.pdf). This isn't too important for what I was trying to do. The ATtiny26L variations all appear quite similar. The schematic isn't great but it met my needs. I had no idea where the blower button trace is connected since it goes under the IC chip. I also believe that the VCC is connected to the button traces under the chip as well since I can't find them anywhere. I also am unsure if I placed the correct resistors. 

This is the schematic I created though it is likely wrong (Open in a new tab if it doesn't show properly):

<img src="./images/Schematic_HotTub-Control_2023-08-16.svg" alt="Schematic" />

The EasyEDA STD Export is located in ./schematics/SCH_HotTub-Control_2023-08-16.json which should be importable.

Next I started playing with an ESP32 and a Breadboard and determined what the 7 wires coming into the control board actually controlled. I determined the following:

1. VCC (5V)
2. GND
3. Thermistor 1
4. Thermistor 2
5. Blower Toggle
6. Heater 1 Toggle
7. Heater 2 Toggle

The below picture is of my ESP32's power and ground connected to the control board. Also attached are two thermistors and 3 LEDs. When I powered the control board without the other wires I would get "Er1" on the display. This is because it is expecting both Temperature inputs. If you have the first but not the second, it will display "Er2". The two thermisters are wired with a 10K resister. When I powered it up it actually showed the temperature!

I wired the Blower and Heater wires to LEDs with a 220 ohm resister. After plugging in, two LEDs lit up. To verify that these were the Heater Toggles, I changed the temperature on the control board to lower than the current reading. The lights went off. When I pressed the Blower button, the 3rd LED went on. I noticed that when the heater lights are on and I pressed the blower the heater would turn off. This is likely because the blower and heater can't run at the same time due to this being a low power system.

Here are two images of my test setup with the Heater on and the Blower on respectively. One of the two LEDs isn't very bright in the first picture.
<img src="./images/breadboard-green.jpg" alt="Breadboard Green" />
<img src="./images/breadboard-blue.jpg" alt="Breadboard Blue" />

I managed to tap into each of the buttons to read and control them from the ESP32. I managed to get all of this working on the system and within Home Assistant. I will include the diagrams of that wiring setup. However, when I was testing things out, I managed to destroy both my ESP32 and the Control Chip by mistakenly sending 16V to from a different breadboard into them.

## Replacing The Control Board <a name="ReplacingESP32"></a>

I decided to redesign the board and upgrade the parts after my mishap.

Here are the main parts I used:</br>
* ESP32S WROOM: [Amazon Link]( https://a.co/d/6bbM3Vc)</br>
* BMP280: [Amazon Link](https://a.co/d/6KyUadY)</br>
* WaveShare 1.5inch RGB OLED Display: [Amazon Link](https://a.co/d/0Q8Kh4p)</br>
* Snappable Strip Board: [Amazon Link](https://a.co/d/6AuGBLy)</br>
* Level Converter: [Amazon Link](https://a.co/d/anycJwW)</br>
* ADC to I2C: [Amazon Link](https://a.co/d/a8LydwA)</br>
* Gorilla Waterproof Tape: [Amazon Link](https://a.co/d/bN7rRyi)</br>

This is my breadboard version with everything connected. I am missing the 3 pulldown resistors from the low side of the level shifter in the next few pictures. I added these due to the default state of high when the ESP32 is reset. It would cause the heater and blower to turn on until the ESP32 is running. 

<img src="./images/breadboard-box.jpg" alt="BreadBoard Box" width="30%"/>

I made a schematic using Kicad and tried to match the parts properly. Note this does not have the pulldown resistors like the Fritzing version has.

<img src="./images/kicad.png" alt="kicad"/>

Rather than just trying to solder everything onto a stripboard I attempted to plan it all out using Fritzing. I mapped it out using the breadboard layout. The top of the two stripboards is the bottom as if you were looking directly through it. The square pins are used to mark where the solder pieces wire ends come through. You can access the fritzing image located in schematics/hottub_controller.fzz

<img src="./images/fritzing.png" alt="Fritzing"/>

<br><br>

Here is the top side of the stripboard with everything but the ESP32 soldered on. All wiring on the top of the board is for control. All wiring on the bottom of the board is for power/ground. The three green circles are where I put the 1K pull-down resistors. The second picture shows where I cut the power rail strip depicted with a red line. This allowed me to have both VCC and GND on that rail. You can see the resistors under the pins on the top of the board which are for the buttons. 

<img src="./images/stripboard-top.jpg" alt="StripBoard Top" width="30%"/>
<img src="./images/cut_stripboard.png" alt="StripBoard Cut" width="30%"/>

Here is the bottom side of the stripboard. The red wires are for 3.3V and the blue wires are for 5V.

<img src="./images/stripboard-bottom.jpg" alt="StripBoard Bottom" width="30%"/>

Here is everything attached of my test setup. The header pins under the board are for power except the top most one which is ground. The top pins go directly to the ESP32. The first 4 control the buttons and the rest control the screen. The screen has a power and ground that go on the bottom. This is a good time to skip to the 'ESPHome and HomeAssistant' section to load everything onto the ESP32. Once ESPHome is on it, you can update it remotely.

<img src="./images/stripboard-complete.jpg" alt="StripBoard Complete" width="30%"/>

I had to use a small hacksaw and file to create a larger hole for the screen. The inside view shows that the plastic support rails fit the screen perfectly.

<img src="./images/screen-hole.jpg" alt="Screen Hole" width="30%"/>
<img src="./images/screen-hole-inside.jpg" alt="Screen Hole Inside" width="30%"/>

I 3d printed a new control board to mount everything on which fits in the original case. The STL file is located here: stl-files/control-board.stl. You will likely have to use a file to make the mount holes bigger. I used the soldering iron with a fine tip to make the button holes bigger as well.

Here is a picture of the underside of the stripboard mount. The 4 screws holding the screen on are M2x4s. I soldered the wires directly to the button on the underside and hot glued them on to fasten them. You can also see the 7 wires coming from the mounted screen.

<img src="./images/printed-board-back.jpg" alt="Printed Board Back" width="30%"/>

I had to solder 2 wires to the front of the board for the Upper Temperature Toggle due to not enough space on the bottom. I also filed down the top right of the board in the picture to give more space to the 7 wire band going to the system board.

<img src="./images/printed-board-front.jpg" alt="Printed Board Front" width="30%"/>

Here is the mounted stripboard and all wires connected. I used M3x6 screws to mount it.

<img src="./images/mounted-stripboard.jpg" alt="Mounted Stripboard" width="30%"/>

To fit it into the original case, I chose to cut out areas for the wires and capacitor rather than 3d printing a new one and wasting plastic. I basically eye-balled the amount of space needed.

<img src="./images/case-cut-1.jpg" alt="Case Cut 1" width="30%"/>
<img src="./images/case-cut-2.jpg" alt="Case Cut 2" width="30%"/>
<img src="./images/case-cut-3.jpg" alt="Case Cut 3" width="30%"/>

The green edge of the hole in the picture is around 15.1mm from the side of the case with the two air holes. The red edge of the hole in the picture is around 28mm from the closest side with the screw hole. You can measure your capacitor to make the hole big enough to fit. 

<img src="./images/case-cut-cap.jpg" alt="Case Cut 1" width="30%"/>

Here is the board mounted in the case and turned on to test. 

<img src="./images/case-mounted.jpg" alt="Case Mounted" style="transform:rotate(180deg);" width="30%"/>

Here is the inside with everything mounted. Be careful with the wires so that they don't get stuck under the screw holes.

<img src="./images/full-mounted.jpg" alt="Full Mounted" style="transform:rotate(180deg);" width="30%"/>
<img src="./images/top-mounted.jpg" alt="Top Mounted" style="transform:rotate(90deg);" width="30%"/>

<br><br>

Finally, I put the Waterproof Gorilla tape over it. I used clear package tape upside down right above the screen so I could remove it if needed. I also added black electric tape around the edges so you can't see the sides of the screen. It looks better in person due to the glare of the tap in the picture. It's not the prettiest but it does the job.

<img src="./images/top-complete.jpg" alt="Top Complete"  width="30%"/>

Here it is freshly installed!

<br>

<img src="./images/running.jpg" alt="Running" style="transform:rotate(90deg);" width="40%"/>

<br>
<br>


## ESPHome and Home Assistant <a name="homeassistant"></a>

I am not going to go into the details of setting up ESPHome and HomeAssistant since there are so many tutorials online. 

The configuration yaml file is called esp_hottub_control_v2.yaml. I use a secrets.yaml file, so make sure you include all the variables needed in there for things like your SSID, WifiPassword, API Encryption, etc.

Here are all the controls I have built into it so far:

<img src="./images/homeassistant.png" alt="HomeAssistant"/>

This allows you to toggle the buttons remotely, set any of the safety features and view the status. Since I put the BMP280 inside the case, it is basically monitoring the case temperature. I have been running it for two days when I took this screenshot. The flat parts of the temperature in the charts are because the water level was too low and the internal pressure sensor was off. When the pressue is too low, the heater turns off which I can't detect with the interface. It is a built in safety feature in the system board.

For the Hot Tub Safety Status, the errors are triggered based on the values below them. If they are triggered, the heaters and blower are turned off. You need to hit the reset button to turn them back on.

The Hot Tub Pump Error will get triggered if the temperature doesn't increase over Hot Tub Allowed Heater Temperature in Hot Tub Allowed Heater Time. I may have to fix this, because the reference points (Hot Tub Pump Start Temp and Hot Tub Pump Start Time) are set to 0 right when the hot tub starts before wifi is connected and the time gets updated.

The Hot Tub Overheat Error detects if the temperature of either thermistor exceeds the Hot Tub Overheat Range.

The Hot Tub Thermistor Error detects if the difference between the two thermistors exceeds the Hot Tub Allowed Temperature Difference value.

After two days, everything seems to be running well and I haven't run into any major issues! Feel free to let me know your suggestions. Hoping to hop in and relax soon!
